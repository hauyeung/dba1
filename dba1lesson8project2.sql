CREATE VIEW BigAlbums AS
SELECT Artists.ArtistName AS Artist, Albums.Title AS Album, Albums.NumberOfSongs AS Songs FROM 
Artists INNER JOIN Albums ON Artists.ArtistID = Albums.ArtistID WHERE Albums.NumberOfSongs = (SELECT MAX(Albums.NumberOfSongs) FROM Albums);

DROP VIEW BigAlbums;

CREATE VIEW LongAlbums AS
SELECT Artists.ArtistName AS Artist, Albums.Title AS Album, Albums.NumberOfSongs AS Songs FROM 
Artists INNER JOIN Albums ON Artists.ArtistID = Albums.ArtistID WHERE Albums.NumberOfSongs = (SELECT MAX(Albums.NumberOfSongs) FROM Albums);
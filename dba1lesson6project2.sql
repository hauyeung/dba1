SELECT Purchases.CustomerID, FirstName, LastName, Address, EmailAddress, FavoriteArtistID,
DateOfPurchase, SongID
FROM ATunesCustomers LEFT JOIN Purchases on Purchases.CustomerID = ATunesCustomers.CustomerID LEFT JOIN CustomerAccounts ON CustomerAccounts.CustomerID = Purchases.CustomerID;

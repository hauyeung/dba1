ALTER TABLE Albums ENGINE=MyISAM;
ALTER TABLE Albums ADD COLUMN AlbumDescription TEXT;
ALTER TABLE Albums ADD FULLTEXT ft_albums (AlbumDescription);
SELECT * FROM Albums WHERE MATCH (AlbumDescription) AGAINST ('Blood');
SELECT * FROM Albums WHERE MATCH (AlbumDescription) AGAINST ('studio');
SELECT * FROM Albums WHERE MATCH (AlbumDescription) AGAINST ('abcd');
DROP TABLE IF EXISTS ArticleAuthors;
DROP TABLE IF EXISTS Articles;
DROP TABLE IF EXISTS ArticleCategories;
DROP TABLE IF EXISTS Commenters;
DROP TABLE IF EXISTS Comments;
DROP TABLE IF EXISTS ArticleViews;

-- create statements 

CREATE TABLE ArticleAuthors(
	ArticleAuthorsId INT PRIMARY KEY AUTO_INCREMENT,    
    AuthorName VARCHAR(500),
    EmailAddress VARCHAR(500),
    Bio TEXT
);

CREATE TABLE Articles(
	ArticlesId INT PRIMARY KEY AUTO_INCREMENT,
    ArticleCategoriesId INT, 
    ArticleAuthorsId INT,
    CommentsId INT, 
    Title VARCHAR(500),
    Description TEXT,
    ArticleText TEXT,
    ArticleDate DATE,    
    FOREIGN KEY (ArticleAuthorsId) REFERENCES ArticleAuthors(ArticleAuthorsId),
    FOREIGN KEY (ArticleCategoriesId) REFERENCES ArticleCategories(ArticleCategoriesId),
    FOREIGN KEY (CommentsId) REFERENCES Comments(CommentsId)    
);

CREATE TABLE ArticleCategories(
	ArticleCategoriesId INT PRIMARY KEY AUTO_INCREMENT,
    CategoryName VARCHAR(200)
);

CREATE TABLE Commenters(
	CommentersID INT PRIMARY KEY AUTO_INCREMENT,    
    CommenterName VARCHAR(500),
    EmailAddress VARCHAR(500)
    
);

CREATE TABLE Comments(
	CommentsId INT PRIMARY KEY AUTO_INCREMENT,   
    CommentersId INT, 
    CommentText TEXT,
    FOREIGN KEY (CommentersId) REFERENCES Commenters(CommentersId)
);
CREATE TABLE ArticleViews(
	ArticleViewsId INT PRIMARY KEY AUTO_INCREMENT,
    ArticlesId INT,
    ArticleViewsDateTime DATETIME,
    FOREIGN KEY (ArticlesId) REFERENCES Articles(ArticlesId)
);

ALTER TABLE Articles ADD FULLTEXT INDEX text_index (Description, ArticleText); 
ALTER TABLE Articles ADD FULLTEXT INDEX text_index_2 (ArticleText); 

-- inserts

INSERT INTO ArticleAuthors VALUES ('','Christeen Marcellus','x92otkz@ie0kf7.com','Certified social media expert. Infuriatingly humble bacon fan. Analyst. Incurable music scholar.');
INSERT INTO ArticleAuthors VALUES ('','Jaleesa Muro','x.izf@skmh61.com','Tv geek. Extreme organizer. Travel trailblazer. Thinker. Hipster-friendly beer fanatic.');
INSERT INTO ArticleAuthors VALUES ('','Brittany Peloquin','61lpv.5@bjpjnnut.com','Unapologetic coffee trailblazer. Typical bacon ninja. Infuriatingly humble twitter evangelist. Wannabe zombie guru. Student.');
INSERT INTO ArticleAuthors VALUES ('','Kasandra Myrie','x837t9@n4iuy0803vj.com','Lifelong coffee scholar. Tv guru. General twitter nerd. Problem solver. Friendly web advocate.');
INSERT INTO ArticleAuthors VALUES ('','Izola Delillo','7r8nmigv46u1@3kiaqn.com','General music geek. Certified pop cultureaholic. Future teen idol. Beer advocate.');


INSERT INTO ArticleCategories VALUES ('','grill');
INSERT INTO ArticleCategories VALUES ('','zebra');
INSERT INTO ArticleCategories VALUES ('','beach');
INSERT INTO ArticleCategories VALUES ('','violin');
INSERT INTO ArticleCategories VALUES ('','scar');

INSERT INTO ArticleViews VALUES ('',1,str_to_date('21,11,2014','%d,%m,%Y'));
INSERT INTO ArticleViews VALUES ('',2,str_to_date('18,11,2014','%d,%m,%Y'));
INSERT INTO ArticleViews VALUES ('',3,str_to_date('1,11,2014','%d,%m,%Y'));
INSERT INTO ArticleViews VALUES ('',1,str_to_date('31,10,2014','%d,%m,%Y'));
INSERT INTO ArticleViews VALUES ('',2,str_to_date('21,10,2014','%d,%m,%Y'));

INSERT INTO Articles VALUES ('', 1,1,3,'10 Things Your Competitors Can Teach You About Oracle','10 Things Your Competitors Can Teach You About Oracle','Also, you\'re. It creeping brought, and, moved.',str_to_date('21,1,2014','%d,%m,%Y'));
INSERT INTO Articles VALUES ('', 5,2,3,'How To Solve The Biggest Problems With Twitter','How To Solve The Biggest Problems With Twitter','Also, you\'re. It creeping brought, and, moved.',str_to_date('21,4,2014','%d,%m,%Y'));
INSERT INTO Articles VALUES ('', 3,3,3,'What Will Fanatic Be Like In 100 Years?','What Will Fanatic Be Like In 100 Years?','Fly female set seed there signs them moving.',str_to_date('21,8,2014','%d,%m,%Y'));
INSERT INTO Articles VALUES ('', 4,1,3,'14 Common Misconceptions About Oracle','14 Common Misconceptions About Oracle','Spirit, behold isn\'t in creature.',str_to_date('21,9,2014','%d,%m,%Y'));
INSERT INTO Articles VALUES ('', 2,1,3,'7 Things About Twitter Your Boss Wants To Know','7 Things About Twitter Your Boss Wants To Know','And. Be won\'t life.',str_to_date('21,12,2014','%d,%m,%Y'));

INSERT INTO Commenters VALUES ('','Debby Gaffney','c607yqto24p@fexuwokt.com');
INSERT INTO Commenters VALUES ('','Therese Thornley','p.h_as.8en-0k5v@zk9obluf0d3.com');
INSERT INTO Commenters VALUES ('','Taneka Vallone','lnf1mmz@m5xh9-f6.com');
INSERT INTO Commenters VALUES ('','Lazaro Posada','p-immk@x7pw-h0nsksg.com');
INSERT INTO Commenters VALUES ('','Lindsay Millette','7_zsfxrlr1i@6wqg9155fq4.com');

INSERT INTO Comments VALUES ('',1,'General analyst. Friendly student. Freelance beer aficionado. Total zombie advocate. Organizer.');
INSERT INTO Comments VALUES ('',2,'Hardcore beer fanatic. Reader. Evil alcohol specialist. Infuriatingly humble zombie evangelist. Travel maven.');
INSERT INTO Comments VALUES ('',3,'Wannabe organizer. Alcohol fanatic. Travel guru. Bacon buff. Music lover. Infuriatingly humble tv advocate.');
INSERT INTO Comments VALUES ('',5,'Pop culture expert. Introvert. Professional analyst. Bacon ninja. Web advocate. Wannabe troublemaker.');
INSERT INTO Comments VALUES ('',1,'Amateur internet aficionado. Wannabe creator. Twitter evangelist. Coffee maven. Certified zombie buff. Pop cultureaholic. Incurable writer.');

-- add comment

DROP PROCEDURE IF EXISTS AddComment;

DELIMITER //
CREATE PROCEDURE AddComment(Commenter VARCHAR(500), CommentText TEXT)
BEGIN
DECLARE idcommenter INT;
SELECT CommentersId INTO idcommenter FROM Commenters WHERE CommenterName = Commenter LIMIT 1;
IF idcommenter IS NULL THEN
	INSERT INTO Commenters VALUES ('', Commenter, '');    
    INSERT INTO Comments VALUES ('',LAST_INSERT_ID(), CommentText);
ELSE
	INSERT INTO Comments VALUES ('', idcommenter, CommentText);
END IF;
END//

CALL AddComment('Debby Gaffney','At distant inhabit amongst by. Appetite welcomed interest the goodness boy not. Estimable education for disposing pronounce her. John size good gay plan sent old roof own. Inquietude saw understood his friendship frequently yet. Nature his marked ham wished.');
CALL AddComment('John Smith','He do subjects prepared bachelor juvenile ye oh. He feelings removing informed he as ignorant we prepared. Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold ye. Be call four my went mean. Celebrated if remarkably especially an. Going eat set she books found met aware.');

DELIMITER ;
DROP VIEW IF EXISTS ArticleDisplay;

-- create view

CREATE VIEW ArticleDisplay AS
SELECT Articles.Title, ArticleCategories.CategoryName, ArticleAuthors.AuthorName 
FROM Articles INNER JOIN ArticleAuthors ON Articles.ArticleAuthorsId = ArticleAuthors.ArticleAuthorsId 
INNER JOIN ArticleCategories ON Articles.ArticleCategoriesId = ArticleCategories.ArticleCategoriesId;

-- 3 queries

SET @currank=0;
SELECT @currank:=@currank+1 AS Rank, ArticleText FROM Articles WHERE MATCH (Description, ArticleText) AGAINST ('Competitors' WITH QUERY EXPANSION);
SELECT *,@currank:=@currank+1 AS Rank FROM Articles WHERE MATCH (Description, ArticleText) AGAINST ('Misconception' WITH QUERY EXPANSION);


SELECT Articles.Title,
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) >= 0) AND (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) <= 10) THEN 1 ELSE 0 END) AS 'D10',
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) >= 11) AND (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) <= 20) THEN 1 ELSE 0 END) AS 'D20',
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) >= 21) AND (SELECT DATEDIFF(NOW(), ArticleViewsDateTime) <= 30) THEN 1 ELSE 0 END) AS 'D30'
FROM ArticleViews
INNER JOIN Articles ON ArticleViews.ArticlesId = Articles.ArticlesId GROUP BY Articles.Title;

SELECT Articles.Title, COUNT(Articles.Title) AS Count FROM ArticleViews
INNER JOIN Articles ON Articles.ArticlesId = ArticleViews.ArticlesId 
GROUP BY ArticleViews.ArticlesId ORDER BY Count DESC LIMIT 1;


USE hauyeung;
CREATE TABLE Artists(
	ArtistID INT PRIMARY KEY,
    ArtistName VARCHAR(200),
    BandWeBURL VARCHAR(1500)
);
SELECT *, 'no' AS QueryExpansion, 'tracks' AS Query FROM Albums WHERE MATCH(AlbumDescription) AGAINST ('tracks')
UNION
SELECT *, 'no' AS QueryExpansion, 'studio' AS Query  FROM Albums WHERE MATCH(AlbumDescription) AGAINST ('studio')
UNION
SELECT *,'yes' AS QueryExpansion, 'tracks' AS Query FROM Albums WHERE MATCH(AlbumDescription) AGAINST ('tracks' WITH QUERY EXPANSION)
UNION
SELECT *, 'yes' AS QueryExpansion, 'studio' AS Query  FROM Albums WHERE MATCH(AlbumDescription) AGAINST ('studio' WITH QUERY EXPANSION);
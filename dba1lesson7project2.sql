SET SQL_SAFE_UPDATES = 0;
UPDATE CustomerAccounts SET CurrentCredit = 
CASE WHEN CurrentCredit >= 1 AND CurrentCredit <= 15 THEN CurrentCredit + 1
WHEN CurrentCredit >= 16 AND CurrentCredit <= 30 THEN CurrentCredit + 2
WHEN CurrentCredit >= 31 THEN CEIL(CurrentCredit * 1.1 )
END;


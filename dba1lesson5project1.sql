SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SELECT * FROM ATunesCustomers;
START TRANSACTION;
DELETE FROM ATunesCustomers WHERE CustomerID = 1 AND FavoriteArtistID = 1;
UPDATE ATunesCustomers SET FirstName = 'Jane' WHERE CustomerID = 1 AND FavoriteArtistID = 2;
SELECT * FROM ATunesCustomers;
ROLLBACK;
SELECT * FROM ATunesCustomers;




SELECT Albums.Title,
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), DateOfPurchase) >= 0) AND (SELECT DATEDIFF(NOW(), DateOfPurchase) <= 30) THEN 1 ELSE 0 END) AS 'D30',
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), DateOfPurchase) >= 31) AND (SELECT DATEDIFF(NOW(), DateOfPurchase) <= 60) THEN 1 ELSE 0 END) AS 'D60',
SUM(CASE WHEN (SELECT DATEDIFF(NOW(), DateOfPurchase) >= 61) AND (SELECT DATEDIFF(NOW(), DateOfPurchase) <= 90) THEN 1 ELSE 0 END) AS 'D90'
FROM Purchases INNER JOIN Songs ON Purchases.SongID = Songs.SongID INNER JOIN Albums ON Albums.AlbumID = Songs.AlbumID
GROUP BY Albums.Title
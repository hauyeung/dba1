USE hauyeung;

create table Albums(
	AlbumId int auto_increment primary key not null,
    ArtistID int,
    YearReleased int,
    Rating int,
    NumberOfSongs int,
    foreign key (ArtistID) references Artists(ArtistID)
);

create table Songs(
	SongID int auto_increment primary key not null,
    AlbumID int,
    TrackNumber int,
    SongName varchar(200),
    foreign key (AlbumID) references Albums(AlbumID)
);

create table ATunesCustomers(
	CustomerID int auto_increment primary key not null,
    FirstName varchar(200),
    LastName varchar(200),
    Address varchar(500),
    EmailAddress varchar(500),
    FavoriteArtistID int,
    foreign key (FavoriteArtistID) references Artists(ArtistID)
);

create table Purchases(
	CustomerID int,
    DateOfPurchase datetime,
    SongID int,
    foreign key (CustomerID) references ATunesCustomers(CustomerID)    ,
    foreign key (SongID) references Songs(Songid)
);

create table CustomerAccounts(
	CustomerID int,
    CurrentCredit int,
    foreign key (CustomerID) references ATunesCustomers(CustomerID)
);

alter table Albums modify column YearReleased year(4);
alter table Artists  engine=InnoDB;
alter table Albums  engine=InnoDB;
alter table ATunesCustomers  engine=InnoDB;
alter table Songs  engine=InnoDB;
alter table Purchases  engine=InnoDB;
alter table CustomerAccounts  engine=InnoDB;

alter table Purchases add primary key (CustomerID,  SongID);
alter table CustomerAccounts add primary key (CustomerID);
alter table Albums add column Title varchar(500);




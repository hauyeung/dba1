SELECT ATunesCustomers.FirstName, ATunesCustomers.LastName
FROM Purchases 
LEFT JOIN ATunesCustomers 
ON ATunesCustomers.CustomerID = Purchases.CustomerID 
GROUP BY ATunesCustomers.CustomerID ORDER BY COUNT(ATunesCustomers.CustomerID)  DESC 
LIMIT 0,5
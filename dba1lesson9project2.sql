DROP PROCEDURE IF EXISTS AddAlbum;
DELIMITER //
CREATE PROCEDURE AddAlbum(NameOfArtist VARCHAR(200), AlbumName VARCHAR(500))
BEGIN
DECLARE artist_count INT;
DECLARE artist_id INT;
SELECT 
    COUNT(*)
INTO artist_count FROM
    Artists
WHERE
    ArtistName = NameOfArtist
LIMIT 0 , 1;
IF artist_count  = 0 then
INSERT INTO Artists (ArtistName) VALUES (NameOfArtist);
SELECT 
    LAST_INSERT_ID()
INTO artist_id;
ELSEIF artist_count = 1 THEN
SELECT ArtistId INTO artist_id FROM Artists WHERE ArtistName = NameOfArtist LIMIT 0,1;
END IF; 
INSERT INTO Albums (ArtistID, Title)  VALUES (artist_id, AlbumName);
END
//
DELIMITER ;
CALL AddAlbum('Madonna','Crossword');
CALL AddAlbum('Celine Dion','One Heart');
DELIMITER //
CREATE PROCEDURE AddNewAlbum(NameOfArtist VARCHAR(200), AlbumName VARCHAR(500))
BEGIN
DECLARE IdArtist INT(11);
SELECT 
    ArtistID
INTO IdArtist FROM
    Artists
WHERE
    ArtistName = NameOfArtist
LIMIT 0 , 1;
INSERT INTO Albums (ArtistID, Title) VALUES (IdArtist, AlbumName);
END
//
CALL AddNewAlbum('Bob Dylan', 'Street Legal')


use hauyeung;
describe Artists;
describe Albums;
describe ATunesCustomers;
describe Songs;
describe Purchases;
describe CustomerAccounts;
insert into Artists (ArtistName, BandWebURL) values ('Bob Dylan', 'www.bobdylan.com');
insert into Artists (ArtistName, BandWebURL) values ('Bono', 'www.bono.com');
insert into Artists (ArtistName, BandWebURL) values ('Madonna', 'www.madonna.com');
insert into Artists (ArtistName, BandWebURL) values ('Harry Belafonte', 'www.harruybelafonte.com');
insert into Artists (ArtistName, BandWebURL) values ('Joyce Irby', 'www.joyceirby.com');

insert into Albums (ArtistID, YearReleased, Title, Rating, NumberOfSongs) values (1, 1966, 'Blonde on Blonde', 5, 8);
insert into Albums (ArtistID, YearReleased, Title, Rating, NumberOfSongs) values (1, 1962, 'Bob Dylan', 4, 8);
insert into Albums (ArtistID, YearReleased, Title, Rating, NumberOfSongs) values (1, 1970, 'New Morning', 5, 9);
insert into Albums (ArtistID, YearReleased, Title, Rating, NumberOfSongs) values (2, 1993, 'Stay', 5, 10);
insert into Albums (ArtistID, YearReleased, Title, Rating, NumberOfSongs) values (2, 1970, 'In the Name of the Father', 5, 8);

insert into Songs (AlbumID, TrackNumber, SongName) values (1, 1, 'Rainy Day Women #12 and 35');
insert into Songs (AlbumID, TrackNumber, SongName) values (1, 2, 'Pledging My Time');
insert into Songs (AlbumID, TrackNumber, SongName) values (1, 3, 'One of Us Must Know (Sooner or Later)');
insert into Songs (AlbumID, TrackNumber, SongName) values (2, 1, 'You are No Good');
insert into Songs (AlbumID, TrackNumber, SongName) values (3, 1, 'If Not for You');

insert into ATunesCustomers (FirstName, lastName, Address, EmailAddress, FavoriteArtistID) values ('John', 'Smith', '9581 Burning Trace, Weyauwega, New Hampshire', 'DEtten367@gustr.com',1);
insert into ATunesCustomers (FirstName, lastName, Address, EmailAddress, FavoriteArtistID) values ('Jerrold', 'Bellamy', '3250 Silent Moor, Little Hope, Michigan', 'detten367@einrot.com',2);
insert into ATunesCustomers (FirstName, lastName, Address, EmailAddress, FavoriteArtistID) values ('Lindy', 'Deering', '3266 Hidden Farm, Hog Jaw, Montana', 'Gamer@gmail.com',3);
insert into ATunesCustomers (FirstName, lastName, Address, EmailAddress, FavoriteArtistID) values ('Aubrie', 'Smith', '8641 Grand Hickory Ledge, Ketchikan,  New Hampshire', 'TheDreary@gmail.com',5);
insert into ATunesCustomers (FirstName, lastName, Address, EmailAddress, FavoriteArtistID) values ('Lori', 'Ready', '9588 Clear Quail Vista, Ninemile Corner, Michigan,', 'TheJaded@hotmail.com',4);

insert into Purchases (CustomerID, DateOfPurchase, SongID) values (1,str_to_date('September 1, 2014','%M %d,%Y'),2);
insert into Purchases (CustomerID, DateOfPurchase, SongID) values (2,str_to_date('October 1, 2014','%M %d,%Y'),4);
insert into Purchases (CustomerID, DateOfPurchase, SongID) values (3,str_to_date('November 13, 2014','%M %d,%Y'),3);
insert into Purchases (CustomerID, DateOfPurchase, SongID) values (1,str_to_date('November 15, 2014','%M %d,%Y'),1);
insert into Purchases (CustomerID, DateOfPurchase, SongID) values (5,str_to_date('November 17, 2014','%M %d,%Y'),2);

insert into CustomerAccounts (CustomerID, CurrentCredit) values  (1, 30);
insert into CustomerAccounts (CustomerID, CurrentCredit) values  (2, 130);
insert into CustomerAccounts (CustomerID, CurrentCredit) values  (3, 220);
insert into CustomerAccounts (CustomerID, CurrentCredit) values  (4, 150);
insert into CustomerAccounts (CustomerID, CurrentCredit) values  (5, 630);